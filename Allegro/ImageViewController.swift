//
//  ImageViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 11.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//


//funkcji na ładowanie zdjęcia na osobnej kolejce używasz wiele razy. Może zrób sobie jakąś pomocniczą klasę Utility z tą funkcją, co?
import UIKit

class ImageViewController: UIViewController {

    var imageUrl: String?
    @IBOutlet weak var image: UIImageView!

   
    func loadImageFromURLUsingBackgroundQueue(completionHandler: @escaping (UIImage?) -> ()) {
        var image: UIImage?
        DispatchQueue.global(qos: .background).async {
            do {
                try image =  UIImage(data: Data(contentsOf: URL(string: (self.imageUrl)!)!))
                DispatchQueue.main.async {
                    completionHandler(image)
                }
            } catch {
                return
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadImageFromURLUsingBackgroundQueue() { fetchedImage in
            self.image.image = fetchedImage
        }
    }
}
