//
//  SearchTableViewCell.swift
//  Allegro
//
//  Created by Marcin Kępa on 11.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    var delegate: SearchViewController?
    var imageUrl: String?
    var item: Item?
    
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var searchName: UILabel!
    @IBOutlet weak var searchPrice: UILabel!
    
    //klikanie w zdjecie i przenoszenie do allegro(search)
    override func awakeFromNib() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(callShowDetailsof))
        tapGestureRecognizer.numberOfTapsRequired = 1
        searchImage.isUserInteractionEnabled = true
        searchImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func loadImageFromURLUsingBackgroundQueue(completionHandler: @escaping (UIImage?) -> ()) {
        var image: UIImage?
        DispatchQueue.global(qos: .background).async {
            do {
                try image =  UIImage(data: Data(contentsOf: URL(string: (self.item?.imageUrl?.first)!)!))
                DispatchQueue.main.async {
                    completionHandler(image)
                }
            } catch {
                return
            }
        }
    }
    
    
    func fillWithData() {
    
        searchName.text = item?.name
        searchPrice.text = String(describing: (item?.price)!)
        
        loadImageFromURLUsingBackgroundQueue() { fetchedImage in
            self.searchImage.image = fetchedImage
        }
    }
    
    func callShowDetailsof() {
        delegate?.showDetailsOf(item)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func showDetailsOf(_ item: Item?) {
        let targetStoryboard = UIStoryboard(name: "Detail", bundle: nil)
        let targetVC = targetStoryboard.instantiateViewController(withIdentifier: "AllegroViewController") as! AllegroViewController
        targetVC.currentItem = item
        navigationController?.pushViewController(targetVC, animated: true)
    }
}



