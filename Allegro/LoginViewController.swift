//
//  LoginViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 16.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//


import UIKit
import SVProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var loginData = [String]()
    
    let user = (login: "marcin", password: "marcin")
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBAction func dismissButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signIn(_ sender: Any) {
        checkIfPasswordsAreCorrect()
    }
    
    @IBAction func forgetPassword(_ sender: Any) {
        createAlert(title: "Send password on your  \nE-mail ? ", message: "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            passwordTextField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
            checkIfPasswordsAreCorrect()
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginTextField.delegate = self
        passwordTextField.delegate = self
        
    
    }
    
    
    private func checkIfPasswordsAreCorrect() {
        
        
        if (loginTextField.text?.isEmpty)! || (passwordTextField.text?.isEmpty)! {
            SVProgressHUD.showError(withStatus: "All fields are required")
        }
        else if loginTextField.text != user.login  || user.password != passwordTextField.text {
            SVProgressHUD.showError(withStatus: "Incorrect authentication data")
            
        }
        else {
            
            Utilities.saveUsernameToKeyChain(userName: loginTextField.text!) //zapis uzytkownika
            
            let targetStoryboard = UIStoryboard(name: "Basket", bundle: nil)
            let targetVC = targetStoryboard.instantiateInitialViewController()
            dismiss(animated: false) {
                UIApplication.shared.keyWindow?.rootViewController?.present(targetVC!, animated: true, completion: nil)
            }
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        loginTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        return true
    }
    
    func createAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField() { textField in
            textField.placeholder = "example@likims.com"
        }
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action in
            if (alert.textFields?.first?.text?.isEmpty)! || !(alert.textFields?.first?.text?.contains("@"))! {
                SVProgressHUD.showError(withStatus: "Wrong Email")
            }
            alert.dismiss(animated: true, completion: nil)
            
        }))
        present(alert, animated: true, completion: nil)
    }
}








