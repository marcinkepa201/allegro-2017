//
//  UITableViewCell.swift
//  Allegro
//
//  Created by Marcin Kępa on 08.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    var currentViewController: CategoriesViewController?
    
    override func awakeFromNib() {
        categoryCollection.delegate = self
        categoryCollection.dataSource = self
    }
    
    var currentCategory: Category?
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var categoryCollection: UICollectionView!
    
    func fillWithData() {
        categoryTitle.text = currentCategory?.title
    }
    
    
}
//"MARK: - Collection View Delegate Methods
extension CategoryTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath)  as? ItemCollectionViewCell
        
        cell?.delegate = currentViewController
        
        cell?.currentItem = currentCategory?.items?[indexPath.row]
        cell?.fillCellWithData()
        return cell!
    }
}

//"MARK: - collection view datasource methods
extension CategoryTableViewCell: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (currentCategory?.items?.count)!
        
    }
}











