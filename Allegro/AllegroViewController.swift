//
//  AllegroViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 10.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class AllegroViewController: UIViewController {
    
    var currentItem: Item?
    
    @IBAction func imageScrollButton(_ sender: Any) {
        let targetSB = UIStoryboard(name: "ImageBrowser", bundle: nil)
        let targetVC = targetSB.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
        targetVC.currentValue = currentItem
        navigationController?.pushViewController(targetVC, animated: true )
    }
    
    @IBOutlet weak var allegroImage: UIImageView!
    @IBOutlet weak var allegroPrice: UILabel!
    @IBOutlet weak var allegroName: UILabel!
    @IBOutlet weak var amountOfPeopleLabel: UILabel!
    @IBOutlet weak var deadlineLabel: UILabel!
    @IBOutlet weak var storageLabel: UILabel!
    
    @IBAction func addLabel(_ sender: Any) {
        Utilities.addItemToBasketPlist(currentItem!)
        let result = Utilities.isUserLoggedIn()
        if result {
            
            let popover = Utilities.prepareViewControllerAsPopOver(from: "Basket", withIdentifier: "BasketNavigationController", anchoredAt: sender as! UIView)
            
            present(popover, animated: true, completion: nil)
            
        }
        else {
            
            let popover = Utilities.prepareViewControllerAsPopOver(from: "Login", withIdentifier: "LoginNavigationController", anchoredAt: sender as! UIView)
            present(popover, animated: true, completion: nil)
        }
    }
    
    @IBAction func buyLabel(_ sender: Any) {
        print("zaloguj sie")
        
    }
    @IBAction func increaseNumberOfElementsInCartButtonPressed(_ sender: Any) {
        if (currentItem?.numbers)! < (currentItem?.storage)!{
            currentItem?.numbers = (currentItem?.numbers)! + 1
            numberLabel.text = String(describing:(currentItem?.numbers)!)
        }
    }
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBAction func subtractionButton(_ sender: Any) {
        if (currentItem?.numbers)! > 0 {
            currentItem?.numbers = (currentItem?.numbers)! - 1
            numberLabel.text = String(describing:(currentItem?.numbers)!)
        }
    }
    
    func loadImage(completionHandler: @escaping (UIImage?) -> ()) {
        var image: UIImage?
        DispatchQueue.global(qos: .background).async {
            do {
                try image =  UIImage(data: Data(contentsOf: URL(string: (self.currentItem?.imageUrl?.first)!)!))
                DispatchQueue.main.async {
                    completionHandler(image)
                }
            } catch {
                return
            }
        }
        
    }
    
    
    func fillOfferWithData() {
        amountOfPeopleLabel.text = String("\(String(describing:(currentItem?.amountOfPeopleBought)!)) Osób kupiło \(String(describing:(currentItem?.amountOfPeopleBought)!)) sztuk")
        deadlineLabel.text = ("\(String(describing: (currentItem?.deadline)!)) dni do końca")
        storageLabel.text = String(" z \(String(describing:(currentItem?.storage)!)) szt.")
        allegroName.text = currentItem?.name
        allegroPrice.text = String("\(String(describing:(currentItem?.price)!)) zł")
        numberLabel.text = String("\(String(describing:(currentItem?.numbers)!))")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillOfferWithData()
        loadImage() { fetchedImage in
            self.allegroImage.image = fetchedImage
        }
        
    }
    
    
}




