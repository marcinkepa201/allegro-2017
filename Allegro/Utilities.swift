//
//  Utilities.swift
//  Allegro
//
//  Created by Marcin Kępa on 18.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class Utilities {//sprawdz czyz zalogowany
    static func isUserLoggedIn() -> Bool {
        
        let path = Bundle.main.path(forResource: "LoginData", ofType:"plist")
        let dict =  NSDictionary(contentsOfFile: path!)
        
        let loginData = dict!.object(forKey: "UserCurentlyLoggedIn") as! String
        if loginData.isEmpty {
            return false
        }
        return true
    }
    //funkcja segue
    static func prepareViewControllerAsPopOver(from storyboard: String, withIdentifier id: String, anchoredAt view: UIView) -> UIViewController {
        let popOverStoryboard = UIStoryboard(name: storyboard, bundle: nil)
        let popoverContent = popOverStoryboard.instantiateViewController(withIdentifier: id)
        popoverContent.modalPresentationStyle = .popover
        
        if let popover = popoverContent.popoverPresentationController {
            let viewForSource = view
            popover.sourceView = viewForSource
            popover.sourceRect = viewForSource.bounds
            popoverContent.preferredContentSize = CGSize(width:200,height:500)
            popover.delegate = self as? UIPopoverPresentationControllerDelegate
        }
        return popoverContent
        
    }
    //zapisz uzytkownika 
    static func saveUsernameToKeyChain(userName: String) {
        let path = Bundle.main.path(forResource: "LoginData", ofType:"plist")
        let dict =  NSMutableDictionary(contentsOfFile: path!)
        
        dict?.setValue(userName, forKey: "UserCurentlyLoggedIn")
        
        dict?.write(toFile: path!, atomically: true)
    }
    
    
    
    
    static func initializeBasketPlist() {
        let path = Bundle.main.resourcePath! + "/Basket.plist"
        NSKeyedArchiver.archiveRootObject([], toFile: path)
    }
    
    static func addItemToBasketPlist(_ item: Item) {
        let path = Bundle.main.resourcePath! + "/Basket.plist"
        var currentBasket = NSKeyedUnarchiver.unarchiveObject(withFile: path) as! [Item]
        currentBasket.append(item)
        NSKeyedArchiver.archiveRootObject(currentBasket, toFile: path)
    }
    
    static func getItemsInBasket() -> [Item] {
        let path = Bundle.main.resourcePath! + "/Basket.plist"
        let currentBasket = NSKeyedUnarchiver.unarchiveObject(withFile: path) as! [Item]
        return currentBasket
    }
    
}





