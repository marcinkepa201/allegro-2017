//
//  ViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 08.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var categoriesWithItems = [Category(title:"Promocje", items:[
                                Item(name:"NARZUTA NA ŁÓŻKO 200x220", price: 42.90 , imageUrl: ["https://c.allegroimg.com/original/01dfc9/48c486ef44be86bc6db82934cfdc","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 3, deadline: "6 ", storage: 1, numbers: 1),
                                Item(name:"Whiskas kurczak 14kg", price: 99.00 , imageUrl: ["https://4.allegroimg.com/original/03c8d6/dea7527e43d5a5096337114daf14","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 54, deadline: "23 ", storage: 300, numbers: 1),
                                Item(name:"Szafka na buty z siedziskiem", price: 169.90 , imageUrl: ["https://8.allegroimg.com/original/0171f2/5dc5cfaf474aaad89473e83045b8","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 12, deadline: "2 ", storage: 10, numbers: 1)]),
                               
                               Category(title:"Elektronika", items:[
                                Item(name:"IPHONE 6 16GB", price: 1199.99, imageUrl: ["https://f.allegroimg.com/original/03ad28/d71fd62745e5bf39747b9471f1bf","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 8, deadline: "7 ", storage: 6, numbers: 1),
                                Item(name:"Myjka ciśnieniowa 1400W", price: 149.00, imageUrl: ["https://0.allegroimg.com/original/01c090/440c54da4f5cba70801721c06de0","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 9, deadline: "5 ", storage: 23, numbers: 1),
                                Item(name:"MP3 Apple iPod shuffle 2GB", price: 239.00, imageUrl: ["https://1.allegroimg.com/original/015933/96b08ae34cba88f60c54bd66fa71","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 56, deadline: "10 ", storage: 81, numbers: 1)]),
                               
                               Category(title:"Samochody", items:[
                                Item(name:"Audi A8 ", price: 40900.00, imageUrl: ["https://8.allegroimg.com/original/03922b/a1d1ee484676b306d13ae39f00f8","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 3, deadline: "32 ", storage: 3, numbers: 1),
                                Item(name:"Mercedes Benz S63 AMG", price: 699000.00, imageUrl: ["https://d.allegroimg.com/original/017feb/c93941b74dd1ba2f1bb6a380fbfd", "https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"],amountOfPeopleBought: 0, deadline: "1 ", storage: 1, numbers: 1),
                                Item(name:"Volvo XC 60", price: 39000.00, imageUrl: ["https://b.allegroimg.com/original/035ec3/a74aaed148039b773fc340340d2b","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 0, deadline: "4 ", storage: 1, numbers: 1)]),
                               
                               Category(title:"Komputery", items:[
                                Item(name:"MacBook Pro 13", price: 6849.00, imageUrl: ["https://5.allegroimg.com/original/014d38/83f469fc49ec9098e80bd2635825","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 4, deadline: "30 ", storage: 2, numbers: 1),
                                Item(name:"Razer Blade", price: 8999.00, imageUrl: ["https://7.allegroimg.com/original/03b6a8/4182c78a4ba0b205444eb6462b77","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 1, deadline: "1 ", storage: 1, numbers: 1),
                                Item(name:"MacBook AIR", price: 3950.00, imageUrl: ["https://1.allegroimg.com/original/0180ef/3a1fda0e4da399c74d14fcf4bd21","https://4.allegroimg.com/original/03cf07/869b9b6b4d4889e5400949003f54"], amountOfPeopleBought: 14, deadline: "8 ", storage: 6, numbers: 1)])
    ]
    
    var filteredData = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        
    }

}
extension CategoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CategoryTableViewCell?
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "InherTableViewCell", for: indexPath) as? InherTableViewCell
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as? CategoryTableViewCell
        }
        cell?.currentViewController = self
        cell?.currentCategory = categoriesWithItems[indexPath.row]
        cell?.fillWithData()
        return cell!
    }

}

extension CategoriesViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return categoriesWithItems.count
    }
}


extension CategoriesViewController: CellDelegate {
    func presentPO(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }

    func showDetailsOf(_ item: Item?) {
        let targetStoryboard = UIStoryboard(name: "Detail", bundle: nil)
        let targetVC = targetStoryboard.instantiateViewController(withIdentifier: "AllegroViewController") as! AllegroViewController
        targetVC.currentItem = item
        navigationController?.pushViewController(targetVC, animated: true)
    }
}

extension CategoriesViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        for category in categoriesWithItems {
            for item in category.items! {
                if item.name?.lowercased().range(of: (searchBar.text?.lowercased())!) != nil {
                    filteredData.append(item)
                }
            }
        }
        let targetStoryboard = UIStoryboard(name: "Search", bundle: nil)
        let targetVC = targetStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        targetVC.items = filteredData
        filteredData = []
        navigationController?.pushViewController(targetVC, animated: true)
    }
    
}




