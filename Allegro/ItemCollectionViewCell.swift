//
//  ItemCollectionViewCell.swift
//  Allegro
//
//  Created by Marcin Kępa on 09.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    
    @IBAction func itemNamePressed(_ sender: Any) {
        delegate?.showDetailsOf(currentItem)
    }
    
    var delegate: CellDelegate?
    
    @IBAction func addItemToBasket(_ sender: Any) {
        
        //logowanie z zapisem
        let result = Utilities.isUserLoggedIn()
        if result {
            
            let popover = Utilities.prepareViewControllerAsPopOver(from: "Basket", withIdentifier: "BasketNavigationController", anchoredAt: sender as! UIView)
            delegate?.presentPO(vc: popover)
        }
        else {
            
            let popover = Utilities.prepareViewControllerAsPopOver(from: "Login", withIdentifier: "LoginNavigationController", anchoredAt: sender as! UIView)
            delegate?.presentPO(vc: popover)
        }

    
    }
    @IBOutlet weak var itemName: UIButton!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    
    var currentItem: Item?
    
    func fillCellWithData() {
        
        itemName.setTitle(currentItem?.name, for: .normal)
        itemPrice.text = String("\(String(describing:(currentItem?.price)!))zł")
        
        loadImage() { fetchedImage in
            self.itemImage.image = fetchedImage
            
        }
        itemImage.layer.masksToBounds = true
        itemImage.layer.cornerRadius = 15.0// co to ma do wypełnianai danymi? daj do innej funkcji
    }
    
    func loadImage(completionHandler: @escaping (UIImage?) -> ()) {
        var image: UIImage?
        DispatchQueue.global(qos: .background).async {
            do {
                try image =  UIImage(data: Data(contentsOf: URL(string: (self.currentItem?.imageUrl?.first)!)!))
                DispatchQueue.main.async {
                    completionHandler(image)
                }
            } catch {
                return
            }
        }
    }
}

protocol CellDelegate {
    func showDetailsOf(_ item: Item?)
    
    func presentPO(vc: UIViewController)
   
    
}






