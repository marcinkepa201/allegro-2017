//
//  PageViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 11.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    var currentValue: Item?
    
    lazy var viewControllersList: [UIViewController] = {
        
        let sb =  UIStoryboard(name: "ImageBrowser", bundle: nil)
        
        var vcs: [UIViewController] = []
        
        for i in 0..<(self.currentValue?.imageUrl?.count)! {
            let vc = sb.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
            vc.imageUrl = self.currentValue?.imageUrl?[i]
            vcs.append(vc)
        }
        return vcs
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
      
        if let firstViewController = viewControllersList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllersList.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard viewControllersList.count > previousIndex else {
            return nil
        }
        
        return viewControllersList[previousIndex]
    }
    
    var originalProperties: NavigationControllerProperties!
    
    struct NavigationControllerProperties {
        var backgroundImage: UIImage?
        var shadowImage: UIImage?
        var isTranslucent: Bool?
    }
    
    let newProperties = NavigationControllerProperties(backgroundImage: UIImage(), shadowImage: UIImage(), isTranslucent: true)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        originalProperties = NavigationControllerProperties(backgroundImage: navigationController?.navigationBar.backgroundImage(for: .default), shadowImage: navigationController?.navigationBar.shadowImage, isTranslucent: navigationController?.navigationBar.isTranslucent)
        styleNavbarWith(properties: newProperties)
        
    }
    
    func styleNavbarWith(properties: NavigationControllerProperties) {
        navigationController?.navigationBar.setBackgroundImage(properties.backgroundImage, for: .default)
        navigationController?.navigationBar.shadowImage = properties.shadowImage
        navigationController?.navigationBar.isTranslucent = properties.isTranslucent ?? false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        styleNavbarWith(properties: originalProperties)
    }

    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllersList.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = vcIndex + 1
        guard viewControllersList.count != nextIndex else {
            return nil
        }
        guard viewControllersList.count > nextIndex else {
            return nil
        }
        return viewControllersList[nextIndex]
        
    }
    
}

