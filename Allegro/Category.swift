//
//  Category.swift
//  Allegro
//
//  Created by Marcin Kępa on 08.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import Foundation

class Category {
    var title: String?
    var items: [Item]?
    
    init(title: String?, items: [Item]?){
        self.title = title
        self.items = items
    }
}

