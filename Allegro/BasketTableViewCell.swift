//
//  BasketTableViewCell.swift
//  Allegro
//
//  Created by Marcin Kępa on 17.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//


import UIKit

class BasketTableViewCell: UITableViewCell {
    
    var currentItem: Item?
    
    @IBOutlet weak var basketImage: UIImageView!
    @IBOutlet weak var basketNabemLabel: UILabel!
    @IBOutlet weak var basketPrice: UILabel!
    
    @IBOutlet weak var basketPrice2: UILabel!
    
    
    func loadImage(completionHandler: @escaping (UIImage?) -> ()) {
        var image: UIImage?
        DispatchQueue.global(qos: .background).async {
            do {
                try image =  UIImage(data: Data(contentsOf: URL(string: (self.currentItem?.imageUrl?.first)!)!))
                DispatchQueue.main.async {
                    completionHandler(image)
                }
            } catch {
                return
            }
        }
    }
    
    func fillOfferWithData() {
        
        basketNabemLabel.text = currentItem?.name
        basketPrice.text = String(describing: (currentItem?.price)!) + "zł"
        basketPrice2.text = String(describing: (currentItem?.price)!) + "zł"
        loadImage() { fetchedImage in
            self.basketImage.image = fetchedImage
        }
        
    }
 
}
