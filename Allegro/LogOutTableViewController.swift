//
//  LogOutViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 22.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class LogOutTableViewController: UITableViewController {

    
    @IBAction func LogOut(_ sender: Any) {
        
        Utilities.saveUsernameToKeyChain(userName: "")
        navigationController?.popViewController(animated: true)
       
    }

        
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

