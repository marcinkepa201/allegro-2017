//
//  Item.swift
//  Allegro
//
//  Created by Marcin Kępa on 08.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import Foundation

class Item: NSObject, NSCoding {
    var name: String?
    var price: Double?
    var imageUrl: [String]?
    var amountOfPeopleBought: Int?
    var deadline: String?
    var storage: Int?
    var numbers: Int?
    
    var description2: String {
        return "name: \(String(describing: name))"
        + "price: \(String(describing: price))"
      //  + "imageUrl: \(String(describing: imageUrl))"
        + "amountOfPeopleBought: \(String(describing: amountOfPeopleBought))"
        + "deadline: \(String(describing: deadline))"
        + "storage: \(String(describing: storage))"
        + "numbers: \(String(describing: numbers))"
        + "description2: \(String(describing: self.description2))"
        
    }
    
    
    init(name: String?, price: Double?, imageUrl: [String]?, amountOfPeopleBought: Int?, deadline: String?, storage: Int?, numbers: Int?){
        self.name = name
        self.price = price
        self.imageUrl = imageUrl
        self.amountOfPeopleBought = amountOfPeopleBought
        self.deadline = deadline
        self.storage = storage
        self.numbers = numbers
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.price, forKey: "price")
        aCoder.encode(self.imageUrl, forKey: "imageUrl")
        aCoder.encode(self.amountOfPeopleBought, forKey: "amountOfPeopleBought")
        aCoder.encode(self.deadline, forKey: "deadline")
        aCoder.encode(self.storage, forKey: "storage")
        aCoder.encode(self.numbers, forKey: "numbers")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        name = aDecoder.decodeObject(forKey: "name") as? String
        price = aDecoder.decodeObject(forKey: "price") as? Double
        imageUrl = aDecoder.decodeObject(forKey: "imageUrl") as? [String]
        amountOfPeopleBought = aDecoder.decodeObject(forKey: "amountOfPeopleBought") as? Int
        deadline = aDecoder.decodeObject(forKey: "deadline") as? String
        storage = aDecoder.decodeObject(forKey: "storage") as? Int
        numbers = aDecoder.decodeObject(forKey: "numbers") as? Int
    }
}

