//
//  BasketTableViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 18.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class BasketTableViewController: UIViewController {
    
    var tableData = [Item]()
    @IBAction func BasketExitButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var orangePrice: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    func sumAllPrices() -> Double{
        var sum = 0.0
        for element in tableData{
            sum += element.price!
        }
        return sum
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    
        
        //zapisywanie do .plist
        tableData = Utilities.getItemsInBasket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        orangePrice.text = String("Podsumuj | \(String(describing:(sumAllPrices()))) zł")
        tableView.reloadData()
    }
    // MARK: - Table view data source
    
   
}

extension BasketTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableData.count
    }
}

extension BasketTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasketTableViewCell", for: indexPath) as! BasketTableViewCell
        cell.currentItem = tableData[indexPath.row]
        cell.fillOfferWithData()
        //cell.textLabel!.text = tableData[indexPath.row]
        return cell
    }
}
