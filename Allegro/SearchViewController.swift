//
//  SerchViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 11.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController{
    
    var items: [Item]?
        
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension SearchViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items!.count
    }
}

extension SearchViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell", for: indexPath) as? SearchTableViewCell
        cell?.item = items?[indexPath.row]
        cell?.fillWithData()
        cell?.delegate = self 
        return cell!
    }
}

