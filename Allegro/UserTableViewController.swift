//
//  UserViewController.swift
//  Allegro
//
//  Created by Marcin Kępa on 22.08.2017.
//  Copyright © 2017 Marcin Kępa. All rights reserved.
//

import UIKit

class UserTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage()

    }
    
    private func addNavBarImage() {
        
        let image = UIImage(named: "photo")
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navigationController?.navigationBar.frame.size.width
        let bannerHeight = navigationController?.navigationBar.frame.size.height
        
        let bannerX = bannerWidth! / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight! / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth!, height: bannerHeight!)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
        
    }
    
    
}
